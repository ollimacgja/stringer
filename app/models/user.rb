class User < ActiveRecord::Base
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable

  attr_accessor :login

  validates :username, :presence => true, :uniqueness => { :case_sensitive => false }

  has_many :posts
  has_many :notifications
  has_many :active_relations, class_name:  "Relation",
                                   foreign_key: "follower_id",
                                   dependent:   :destroy
  has_many :passive_relations, class_name:  "Relation",
                                   foreign_key: "followed_id",
                                   dependent:   :destroy
  has_many :following, through: :active_relations, source: :followed
  has_many :followers, through: :passive_relations

  mount_uploader :avatar, AvatarUploader

  scope :by_username, -> username { where("username ilike (?)","%#{username}%") }

  def self.find_for_database_authentication(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions.to_h).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      where(conditions.to_h).first
    end
  end

  def follow(user_follow)
    active_relations.create(followed_id: user_follow.id)
    Notification.create(user: user_follow, follower: self)
  end

  def unfollow(user_unfollow)
    active_relations.find_by_followed_id(user_unfollow.id).destroy
  end

  def following?(target_user)
    following.include?(target_user)
  end

  def picture
    avatar.url.present? ? avatar.try(:url): 'avatar.jpg'
  end

  def newsfeed
    ids = [id] + following.pluck(:id)
    Post.where('user_id in (?)', ids).order('created_at desc')
  end


end
