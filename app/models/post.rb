class Post < ActiveRecord::Base
  belongs_to :user
  validates_presence_of :content, :user
  validates_length_of :content, :maximum => 255
end
