class UsersController < ApplicationController

	has_scope :by_username

	def show
		@user = User.find_by_username(params[:username])
		redirect_to authenticated_root_path unless @user.present?
	end

	def followers
		@user = User.find_by_username(params[:username])
		if @user.present?
			@followers = @user.followers
		else
			redirect_to authenticated_root_path
		end
	end

	def following
		@user = User.find_by_username(params[:username])
		if @user.present?
			@following = @user.following
		else
			redirect_to authenticated_root_path
		end
	end

	def search
		@users = apply_scopes(User).all
	end
end
