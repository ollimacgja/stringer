class NotificationsController < ApplicationController
	before_action :authenticate_user!
	
	def index
		@user = User.find params[:user_id]
		@notifications = @user.notifications.order('created_at desc')
		@notifications.where(read_at: nil).update_all(read_at: DateTime.current)
	end
end
