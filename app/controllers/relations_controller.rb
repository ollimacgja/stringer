class RelationsController < ApplicationController
	before_action :authenticate_user!
	def create
		followed = User.find params[:followed_id]
		current_user.follow(followed)
		redirect_to :back, notice: "You are now following @#{followed.username}"
	end

	def destroy
		followed = User.find params[:id]
		current_user.unfollow(followed)
		redirect_to :back, notice: "You are not following @#{followed.username} anymore"
		
	end
end
