class PostsController < ApplicationController
	before_action :authenticate_user!
	
	def create
		post = Post.new(post_params)
		if post.save
			flash[:notice] = 'Stringed it successfully!'
		else
			flash[:alert] = post.errors.full_messages
		end
			redirect_to authenticated_root_path
	end

	def destroy
		post = Post.find params[:id]
		if post.destroy
			flash[:alert] = 'String deleted!'
		end
			redirect_to authenticated_root_path		
	end

	private
    def post_params
      params.require(:post).permit(:content, :user_id)
    end
end
