require 'rails_helper'

RSpec.describe User, type: :model do
  let(:user_a) {FactoryGirl.create(:user, :camillo)}
  let(:user_b) {FactoryGirl.create(:user, :eduardo)}

  describe 'finding a user' do

  	it "can find by email" do
    	expect(User.find_for_database_authentication( {login: user_a.email} )).to eq(user_a)
  	end

  	it "can find by username" do
    	expect(User.find_for_database_authentication( {login: user_a.username} )).to eq(user_a)
  	end

  	it "can find by email parameter" do
    	expect(User.find_for_database_authentication( {email: user_a.email} )).to eq(user_a)
  	end
  end

  describe 'relations' do
  		before(:each) {user_a.follow(user_b) }

  	context '#follow' do
  		context	'User_a' do 
  		  it { expect(user_a.following).to include(user_b)}

  		  it '#following?' do
  		  	expect(user_a.following?(user_b)).to be true
  		  end
  		end

  		context 'User_b' do
  			it { expect(user_b.followers).to include(user_a)}
  		end
  	end

  	context '#unfollow' do
  		before(:each) { user_a.unfollow(user_b)}
  		context	'User_a' do 
  		  it { expect(user_a.following).not_to include(user_b)}

  		  it '#following?' do
  		  	expect(user_a.following?(user_b)).to be false
  		  end
  		end

  		context 'User_b' do
  			it { expect(user_b.followers).not_to include(user_a)}
  		end
  	end

  end

  describe '#picture' do
    context 'has an image' do
      it { expect(user_a.picture).to eq("#{Rails.root}/spec/support/uploads/user/avatar/#{user_a.id}/background_g.jpg")}
    end

    context 'has no image' do
      it { expect(user_b.picture).to eq('avatar.jpg')}
    end
  end

  describe '#newsfeed' do
    let(:post_a) {user_a.posts.create(content: "User A Post")}
    let(:post_b) {user_b.posts.create(content: "User B Post")}
    
    before do 
      user_a.follow(user_b)
    end

    context "User A" do
      it { expect(user_a.newsfeed).to include(post_a)}
      it { expect(user_a.newsfeed).to include(post_b)}
    end

    context "User B" do
      it { expect(user_b.newsfeed).not_to include(post_a)}
    end

  end
end