require 'rails_helper'

RSpec.describe Notification, type: :model do
  it {should belong_to(:user)}
  it {should belong_to(:follower)}
  it {should validate_presence_of(:user)}
  it {should validate_presence_of(:follower)}
end
