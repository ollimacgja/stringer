FactoryGirl.define do
  factory :user do
    username "MyString"
		email "my@email.com"
		password '12345678'

		trait :camillo do
			username 'ollimacgja'
			email 'ollimacgja@gmail.com'
			avatar { Rack::Test::UploadedFile.new(File.join(Rails.root, 'app', 'assets', 'images','background_g.jpg')) }
		end

		trait :eduardo do
			username 'eduardo_helabs'
			email 'eduardo@helabs.com'
		end
  end

end
