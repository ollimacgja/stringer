require 'rails_helper'

RSpec.describe RelationsController, type: :controller do
	let(:user_a) {FactoryGirl.create(:user, :camillo)}
	let(:user_b) {FactoryGirl.create(:user, :eduardo)}
	before(:each) do
		@request.env['HTTP_REFERER'] = 'http://localhost:3000/users/search'
		sign_in user_a
	end

	describe 'POST #create' do

		before do
			
			post :create, followed_id: user_b.id
		end

		it { should set_flash[:notice].to("You are now following @#{user_b.username}") }
		it { should redirect_to('http://localhost:3000/users/search') }

	end

	describe 'delete #destroy' do
		before do
			user_a.follow(user_b)
			delete :destroy, id: user_b.id
		end


		it { should set_flash[:notice].to("You are not following @#{user_b.username} anymore") }
		it { should redirect_to('http://localhost:3000/users/search') }
	end
end
