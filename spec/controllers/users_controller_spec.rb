require 'rails_helper'

RSpec.describe UsersController, type: :controller do
	let(:user) {FactoryGirl.create(:user, :camillo)}
	
	describe 'GET Show' do
		context 'existing user' do

			before { get :show, username: user.username }

			it { should render_template('show') }

		end

		context 'non existing user' do

			before { get :show, username: 'eduardo_helabs' }

			it { should redirect_to(authenticated_root_path) }

		end

	end

	describe 'GET followers' do
		context 'existing user' do

			before { get :followers, username: user.username }

			it { should render_template('followers') }

		end

		context 'non existing user' do

			before { get :followers, username: 'eduardo_helabs' }

			it { should redirect_to(authenticated_root_path) }

		end

	end

	describe 'GET following' do
		context 'existing user' do

			before { get :following, username: user.username }

			it { should render_template('following') }

		end

		context 'non existing user' do

			before { get :following, username: 'eduardo_helabs' }

			it { should redirect_to(authenticated_root_path) }

		end

	end

	describe 'GET search' do

		before { get :search }

		it { should render_template('search') }
	end
end
