require 'rails_helper'

RSpec.describe NotificationsController, type: :controller do

	let(:user) {FactoryGirl.create(:user, :camillo)}
	before(:each) {sign_in user}

	describe 'GET Index' do
		before { get :index, user_id: user.id }

		it { should render_template('index') }

	end


end
