require 'rails_helper'

RSpec.describe PostsController, type: :controller do
	let(:user) {FactoryGirl.create(:user, :camillo)}
	let(:post_a) { user.posts.create(content: "Some content")}
	before(:each) {sign_in user}

	describe 'POST #create' do


		context	'valid Post' do
			before { post :create, post: {content: 'Some content', user_id: user.id} }

			it { should set_flash[:notice].to('Stringed it successfully!') }
			it { should redirect_to(authenticated_root_path) }

		end

		context 'invalid Post' do
			before { post :create, post: { user_id: user.id} }

			it { should set_flash[:alert] }
			it { should redirect_to(authenticated_root_path) }
		end

	end

	describe 'delete #destroy' do
		before { delete :destroy, id: post_a }


		it { should set_flash[:alert] }
		it { should redirect_to(authenticated_root_path) }
	end

end
