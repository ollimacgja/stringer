class CreateNotifications < ActiveRecord::Migration
  def change
    create_table :notifications do |t|
      t.references :user, index: true
      t.integer :follower_id
      t.datetime :read_at

      t.timestamps null: false
    end
    add_foreign_key :notifications, :users
  end
end
